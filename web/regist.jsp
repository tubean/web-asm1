<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Regist new account</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="resources/css/bootstrap.css">
</head>
<body>
<%@ include file="header.jsp" %>
<br/><br/><br/>
<center><h1>Login</h1></center>
<br/><br/><br/>
    <form action="main.jsp" method="POST">
        <table align="center">
            <tr>
                <th alight = "right">Username:</th>
                <td><input type ="text" name = "txtusername" class="input-group" placeholder="Username"></td>
            </tr>
            <tr>
                <th align="right">Password:</th>
                <td><input type="password" name="txtpassword" class="input-group" placeholder="Password"></td>
            </tr>
            <tr>
                <td colspan="2" align="right"><input type="submit" value="Log In" class="btn btn-primary"></td>
            </tr>
        </table>
    </form>
<%@ include file="footer.jsp" %>
</body>
</html>
