<%--
  Created by IntelliJ IDEA.
  User: NWS_TUDA_PC
  Date: 8/29/2018
  Time: 10:54 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Exercise 3</title>
</head>
<body>
<div style="width: 500px; margin: 30px auto;">
    <h2>Đăng ký môn học</h2>
    <form action="resultObjects.jsp" method="POST" target="_blank">
        <div>
            <label>Tên </label>
            <input type="text" name="firstname" value=""/>
        </div>
        <br/>
        <div>
            <label>Họ </label>
            <input type="text" name="surname" value=""/>
        </div>
        <br/>
        <div>
            <label>Số điện thoại </label>
            <input type="text" name="phone" value=""/>
        </div>
        <br/>
        <div>
            <label>Email </label>
            <input type="text" name="email" value=""/>
        </div>
        <br/>
        <input type="checkbox" name="maths"/> Toán
        <input type="checkbox" name="physics"/> Lý
        <input type="checkbox" name="chemistry"/> Hóa
        <br/>
        <div>
            <input type="submit" name="submit" value="Đăng ký"/>
        </div>
    </form>
    <a href="index.jsp">Back</a>
</div>
</body>
</html>
