<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Show detail student</title>
</head>
<body>
<h2>Kết quả đăng ký môn học</h2>
<ul style="align-content: center">
    <li><p><b>Họ</b>
        <%= request.getParameter("firstname")%>
    </p></li>
    <li><p><b>Tên:</b>
        <%= request.getParameter("surname")%>
    </p></li>
    <li><p><b>Số điện thoại:</b>
        <%= request.getParameter("phone")%>
    </p></li>
    <li><p><b>Email:</b>
        <%= request.getParameter("email")%>
    </p></li>
    <li><p><b>Môn học:</b>
        <%if (request.getParameter("maths") != null) %> Toán
        <%if (request.getParameter("physics") != null) %> Lý
        <%if (request.getParameter("chemistry") != null) %> Hóa <br/>
    </p></li>
</ul>
<a href="index.jsp">Home</a>
</body>
</html>
